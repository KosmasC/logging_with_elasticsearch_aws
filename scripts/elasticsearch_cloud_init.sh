#!/bin/sh

set -e

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

sudo apt-get update && sudo apt install apt-transport-https

echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

sudo apt-get update && sudo apt-get install elasticsearch

# Configure elasticsearch
sed -i '/node.name/s/^#*\s*//g' /etc/elasticsearch/elasticsearch.yml
sed -i 's/network.host: 192.168.0.1/network.host: 0.0.0.0/g' /etc/elasticsearch/elasticsearch.yml 
sed -i '/network.host/s/^#*\s*//g' /etc/elasticsearch/elasticsearch.yml 
sed -i '/discovery.seed_hosts/a'discovery.seed_hosts:' '['"'127.0.0.1'"']'' /etc/elasticsearch/elasticsearch.yml 
sed -i '/cluster.initial_master_nodes/a'cluster.initial_master_nodes:' '['"'node-1'"']'' /etc/elasticsearch/elasticsearch.yml

# Enable and start service
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable elasticsearch.service
sudo /bin/systemctl start elasticsearch.service

# Install Kibana
sudo apt install kibana

# Configure Kibana
sudo sed -i '/server.host:/a'server.host:' ''"'0.0.0.0'"''' /etc/kibana/kibana.yml

# Enable and start service
sudo /bin/systemctl daemon-reload
sudo /bin/systemctl enable kibana.service
sudo /bin/systemctl start kibana.service
