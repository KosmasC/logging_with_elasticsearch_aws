#!/bin/sh

set -e

sudo apt-get update && sudo apt install docker.io -y

# Setup fluent
sudo mkdir /fluent

# fluent.conf file
cat << EOF | sudo tee /fluent/fluent.conf
<source>
  @type forward
  port  24224
  bind  0.0.0.0
</source>

<match *.**>
  @type copy

  <store>
    @type elasticsearch
    host ${es_cluster}
    port 9200
    logstash_format true
    logstash_prefix fluentd
    logstash_dateformat %Y%m%d
    include_tag_key true
    type_name access_log
    tag_key @log_name
    flush_interval 1s
  </store>

  <store>
    @type stdout
  </store>
</match>
EOF

# Dockerfile
cat << EOF | sudo tee /fluent/Dockerfile
FROM fluent/fluentd:v1.12-1

# Use root account to use apk
USER root

# Install elasticsearch plugin
RUN apk add --no-cache --update --virtual .build-deps \
    sudo build-base ruby-dev \
    && sudo gem install fluent-plugin-elasticsearch \
    && sudo gem sources --clear-all \
    && apk del .build-deps \
    && rm -rf /tmp/* /var/tmp/* /usr/lib/ruby/gems/*/cache/*.gem

COPY fluent.conf /fluentd/etc/

USER fluent
EOF

# Build fluent docker image
cd /fluent && sudo docker build -t fluentd-elasticsearch .

sudo docker run --name fluentd -p 24224:24224 -d fluentd-elasticsearch
sudo docker run --name zepa_wordpress -p 80:80 -log-driver=fluentd --log-opt tag="docker.{.ID}}" -d wordpress

