terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
}

resource "aws_vpc" "elk_vpc" {
  cidr_block            = "10.0.0.0/16"
  enable_dns_support    = true
  enable_dns_hostnames  = true
}

resource "aws_subnet" "public_subnet1" {
  cidr_block              = "10.0.0.0/24"
  vpc_id                  = aws_vpc.elk_vpc.id

  tags = {
    Name = "Public Subnet"
  }
}

resource "aws_route_table" "public_route_table" {
  vpc_id      = aws_vpc.elk_vpc.id
  depends_on  = [ aws_internet_gateway.internet_gateway ]

  tags = {
    Name = "Public Route Table"
  }

  route {
    cidr_block  = "0.0.0.0/0"
    gateway_id  = aws_internet_gateway.internet_gateway.id
  }
}

resource "aws_route_table_association" "association_for_public_route_table0" {
  subnet_id       = aws_subnet.public_subnet1.id
  route_table_id  = aws_route_table.public_route_table.id
} 

resource "aws_internet_gateway" "internet_gateway" {
  vpc_id  = aws_vpc.elk_vpc.id
}

resource "aws_instance" "wordpress" {
  ami                         = "ami-08a1a61694dd1c82f"
  instance_type               = "t2.micro"
  key_name                    = "terraform_ssh_pub"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet1.id

  vpc_security_group_ids =  [
                              aws_security_group.allow_web_80.id,
                              aws_security_group.allow_web_443.id,
                              aws_security_group.allow_ssh_22.id
                            ]

  user_data = data.template_file.wordpress_cloud_init.rendered

  tags = {
    Name = "wordpress"
  }
}

resource "aws_instance" "microservices" {
  ami                         = "ami-08a1a61694dd1c82f"
  instance_type               = "t2.micro"
  key_name                    = "terraform_ssh_pub"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet1.id

  vpc_security_group_ids =  [
                              aws_security_group.allow_web_80.id,
                              aws_security_group.allow_web_443.id,
                              aws_security_group.allow_ssh_22.id
                            ]

  user_data = data.template_file.microservices_cloud_init.rendered

  tags = {
    Name = "microservices"
  }
}

resource "aws_instance" "elasticsearch" {
  ami                         = "ami-038389712bd4f9ec0"
  instance_type               = "t2.small"
  key_name                    = "terraform_ssh_pub"
  associate_public_ip_address = true
  subnet_id                   = aws_subnet.public_subnet1.id

  vpc_security_group_ids =  [
                              aws_security_group.allow_web_80.id,
                              aws_security_group.allow_web_443.id,
                              aws_security_group.allow_ssh_22.id,
                              aws_security_group.allow_kibana_5601.id,
                              aws_security_group.allow_elasticsearch_9200.id
                            ]

  user_data = data.template_file.elasticsearch_cloud_init.rendered

  tags = {
    Name = "elasticsearch"
  }
}

resource "aws_security_group" "allow_web_80" {
  name        = "allow_web_80"
  description = "Allow web access inbound traffic in port 80"
  vpc_id      = aws_vpc.elk_vpc.id

  ingress {
    description = "Allow HTTP traffic"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "allow_web_80"
  }
}
 
resource "aws_security_group" "allow_ssh_22" {
  name        = "allow_ssh_22"
  description = "Allow ssh inbound traffic in port 22"
  vpc_id      = aws_vpc.elk_vpc.id

  ingress {
    description = "Allow SSH traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "allow_ssh_22"
  }
} 

resource "aws_security_group" "allow_web_443" {
  name        = "allow_web_443"
  description = "Allow web access inbound traffic in port 443"
  vpc_id      = aws_vpc.elk_vpc.id

  ingress {
    description = "Allow HTTPS traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "allow_web_443"
  }
}

resource "aws_security_group" "allow_kibana_5601" {
  name        = "allow_kibana_5601"
  description = "Allow kibana access inbound traffic in port 5601"
  vpc_id      = aws_vpc.elk_vpc.id

  ingress {
    description = "Allow Kibana access"
    from_port   = 5601
    to_port     = 5601
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "allow_kibana_5601"
  }
}

resource "aws_security_group" "allow_elasticsearch_9200" {
  name        = "allow_elasticsearch_9200"
  description = "Allow elasticsearch access inbound traffic in port 9200"
  vpc_id      = aws_vpc.elk_vpc.id

  ingress {
    description = "Allow Elasticsearch access"
    from_port   = 9200
    to_port     = 9200
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "allow_elasticsearh_9200"
  }
}

resource "aws_key_pair" "terraform_ssh_pub" {
  key_name    = "terraform_ssh_pub"
  public_key  = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC2hvdArgDGl/cDLSQPSRZk7InKv7io42KKnGjmPGAABvnrv6nScbfIfllBJAU2FlAoN9Vyg8ILjWnKTQPqRoWhGEFiYdGdGJ01cVnO9A9J7xdzHfgVq1rs0Wafx5mhhU/lZBvV3mROsLK87kDzz1nZ4So1738lrTOwFO0CsSoiZwu0uH96rf/VCBNQtfa3LZyVZGzN0Ql7nKulngWAHCNQ/lUah2wXKI/FmCV/DucQVujeNP+8SbJ/69VV8ABuxE/v3QaAiopQPssKlTnqDGeUgWB4WdPQ6/+2GsExuljDkjQFxebvmnUabeP4nFZoQXTpsmEtitIr/HUOIfBtHPVx kosmas@kosmas-X1-5"
}

data "template_file" "wordpress_cloud_init" {
  template  = file("scripts/wordpress_cloud_init.sh")
  vars = {
    es_cluster = aws_instance.elasticsearch.private_dns
  }
}

data "template_file" "microservices_cloud_init" {
  template  = file("scripts/microservices_cloud_init.sh")
  vars = {
    es_cluster = aws_instance.elasticsearch.private_dns
  }
}

data "template_file" "elasticsearch_cloud_init" {
  template  = file("scripts/elasticsearch_cloud_init.sh")
}

output "wordpress_public_ip" {
  value       = aws_instance.wordpress.public_ip
  description = "The public IP address of the wordpress instance"
}

output "elasticsearch_cluster_endpoint" {
  value       = "http://${aws_instance.elasticsearch.public_ip}:9200"
  description = "The elastic search cluster endpoint"
}

output "kibana_endpoint" {
  value       = "http://${aws_instance.elasticsearch.public_ip}:5601"
  description = "The Kibana endpoint"
}
